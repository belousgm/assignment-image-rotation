//
// Created by vbox on 01.11.22.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_FORMAT_AND_WORK_H
#define IMAGE_TRANSFORMER_IMAGE_FORMAT_AND_WORK_H
#include "util.h"

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void rotate(struct image from_image, struct image* to_image);

void free_memory(struct image* image);

#endif //IMAGE_TRANSFORMER_IMAGE_FORMAT_AND_WORK_H
