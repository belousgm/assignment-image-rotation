//
// Created by vbox on 01.11.22.
//

#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H
#include <stdint.h>
#include <stdio.h>

enum working_code {
    SUCCESS = 0,
    READ_ERROR,
    WRITE_ERROR,
    OPEN_FILE_ERROR,
    CLOSE_FILE_ERROR,
    TRANSFORMATION_ERROR
};

void print_status(char* text, enum working_code code);

#endif //IMAGE_TRANSFORMER_UTIL_H
