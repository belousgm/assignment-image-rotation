//
// Created by vbox on 01.11.22.
//

#ifndef IMAGE_TRANSFORMER_BMP_FORMAT_AND_WORK_H
#define IMAGE_TRANSFORMER_BMP_FORMAT_AND_WORK_H
#include "image_format_and_work.h"
#include "util.h"

struct __attribute__((packed)) bmp_header;

enum working_code read_image(FILE* const file, struct image* image);
enum working_code input_bmp(FILE* const file, struct bmp_header* bmp_file);
enum working_code set_pixel(FILE* const file, struct image image);

uint8_t get_padding(uint32_t width);

enum working_code write_image(FILE* const file, struct image image);

struct bmp_header create_bmp_header(struct image image);

uint32_t get_bmp_size(struct image image);

#endif //IMAGE_TRANSFORMER_BMP_FORMAT_AND_WORK_H
