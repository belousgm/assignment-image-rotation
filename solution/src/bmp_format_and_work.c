//
// Created by vbox on 01.11.22.
//

#include "bmp_format_and_work.h"
#include <stdlib.h>

#define MAX_PADDINGS 3

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static struct image create_image(uint32_t biWidth, uint32_t biHeight){
    return (struct image) {
        .width = biWidth,
        .height = biHeight,
        .data = malloc(sizeof(struct pixel)*(biWidth)*(biHeight))
    };
}

enum working_code read_image(FILE* const file, struct image* image) {
    struct bmp_header bmp_file = {0};
    enum working_code bmp_create_status = input_bmp(file, &bmp_file);
    if (bmp_create_status != SUCCESS) return bmp_create_status;

    *image = create_image(bmp_file.biWidth, bmp_file.biHeight);

    enum working_code set_pixels_image_status = set_pixel(file, *image);
    if (set_pixels_image_status != SUCCESS) return set_pixels_image_status;

    if (fclose(file) != 0) return CLOSE_FILE_ERROR;

    return SUCCESS;
}

enum working_code input_bmp(FILE* const file, struct bmp_header* bmp_file) {
    if (fread(bmp_file, sizeof(struct bmp_header), 1, file) != 1) return READ_ERROR;
    return SUCCESS;
}

uint8_t get_padding(uint32_t width) {
    uint8_t padding = (width * sizeof(struct pixel)) % 4;
    if (padding == 0) return padding;
    return (4-padding);
}

enum working_code set_pixel(FILE* const file, struct image image) {
    uint64_t height = image.height;
    uint64_t width = image.width;
    uint8_t padding = get_padding(width);

    for (size_t i = 0; i<height; i++) {
        if (fread(image.data+width*i, sizeof(struct pixel), width, file) != width) return READ_ERROR;
        if (fseek(file, padding, SEEK_CUR) != 0) return TRANSFORMATION_ERROR;
    }
    return SUCCESS;
}

enum working_code write_image(FILE* const file, struct image image) {
    struct bmp_header new_bmp = create_bmp_header(image);
    if (fwrite(&new_bmp, sizeof(struct bmp_header), 1, file) != 1) return WRITE_ERROR;

    uint64_t height = image.height;
    uint64_t width = image.width;
    uint8_t padding = get_padding(width);
    uint8_t paddings[MAX_PADDINGS] = {0};

    for (size_t i = 0; i < height; i++) {
        if (fwrite((image.data) + i*width, sizeof(struct pixel)*width, 1, file) != 1) return WRITE_ERROR;
        if ((fwrite(paddings, padding, 1, file) != 1) && (padding != 0)) return WRITE_ERROR;
    }

    free_memory(&image);

    return SUCCESS;
}

struct bmp_header create_bmp_header(struct image image) {
    return (struct bmp_header) {
            .bfType = 0x4D42,
            .bfileSize = sizeof(struct bmp_header) + get_bmp_size(image),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image.width,
            .biHeight = image.height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = get_bmp_size(image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

uint32_t get_bmp_size(struct image image) {
    return ( sizeof(struct pixel) * image.height * image.width + get_padding(image.width) * image.height );
}
