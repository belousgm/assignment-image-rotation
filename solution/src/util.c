//
// Created by vbox on 01.11.22.
//

#include "util.h"

static const char* const errors[] = {
        [SUCCESS] = "Success!",
        [READ_ERROR] = "File read error",
        [WRITE_ERROR] = "File write error",
        [OPEN_FILE_ERROR] = "File open error",
        [CLOSE_FILE_ERROR] = "File close error",
        [TRANSFORMATION_ERROR] = "Error inside the function"
};

void print_status(char* text, enum working_code code) {
    fprintf(stdout, "%s", text);
    fprintf(stderr,"%s\n", errors[code]);
}
