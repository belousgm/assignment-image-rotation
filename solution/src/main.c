#include "bmp_format_and_work.h"
#include "image_format_and_work.h"
#include "util.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Args error\n");
        return 1;
    }

    FILE* input_file = NULL;
    if ((input_file = fopen(argv[1], "rb")) == NULL) {
        fprintf(stderr, "File open error\n");
        return 1;
    }

    struct image input_image = {0};
    enum working_code read_image_status = read_image(input_file, &input_image);
    print_status("Reading process finished: ", read_image_status);
    if (read_image_status != SUCCESS) return 1;

    struct image rotate_image = {0};
    rotate(input_image, &rotate_image);

    free_memory(&input_image);

    FILE* output_file = NULL;
    if ((output_file = fopen(argv[2], "wb")) == NULL) {
        fprintf(stderr, "File open error\n");
        return 1;
    }

    enum working_code write_image_status = write_image(output_file, rotate_image);
    print_status("Writing process finished: ", write_image_status);
    if (write_image_status != SUCCESS) return 1;

    return 0;
}
