//
// Created by vbox on 01.11.22.
//

#include "image_format_and_work.h"
#include <stdlib.h>

static size_t pixel_get(size_t row, size_t column, uint32_t width) {
    return row * width + column;
}

static size_t pixel_set(size_t row, size_t column, uint32_t width) {
    return (column * width) + (width - row - 1);
}

void rotate(struct image from_image, struct image* to_image) {
    to_image->width = from_image.height;
    to_image->height = from_image.width;
    to_image->data = malloc(sizeof(struct pixel)*(to_image->width)*(to_image->height));

    for (size_t row = 0; row < from_image.height; row++) {
        for (size_t column = 0; column < from_image.width; column++) {
            to_image->data[pixel_set(row, column, to_image->width)] = from_image.data[
                    pixel_get(row, column, from_image.width)];
        }
    }
}

void free_memory(struct image* image) {
    free(image->data);
}
